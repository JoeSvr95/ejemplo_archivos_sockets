#include "csapp.h"

enum Estado {EXIT = 0, ENVIANDO_NOMBRE, RECIBIENDO_ARCHIVO} estado; //Estados para la FSM

int main(int argc, char **argv)
{
	int clientfd;
	char *port;
	char *host, buf[MAXLINE];
	char *filename;
	char *filebuf;
	rio_t rio;
	estado = ENVIANDO_NOMBRE;

	int l;
	int tam = 0;
	FILE * fp;

	if (argc != 4) {
		fprintf(stderr, "usage: %s <host> <port> <filename>\n", argv[0]);
		exit(0);
	}
	host = argv[1];
	port = argv[2];

	if ((l = strlen(argv[3])) > 0) {
		filename = Malloc(l + 1); //Guarda el nombre del archivo en el heap con espacio extra para \n
		strcpy(filename,argv[3]); 
		filename[l] = '\n';	
	}else {
		fprintf(stderr, "Error al leer nombre de archivo\n");
		exit(0);
	}

	clientfd = Open_clientfd(host, port);
	Rio_readinitb(&rio, clientfd);
	printf("Pidiendo al servidor el archivo %s", filename);

	while(estado) {
		switch(estado)
		{
			case ENVIANDO_NOMBRE:
				Rio_writen(clientfd, filename, l+1); //Envia el nombre del archivo al servidor
				Rio_readlineb(&rio, buf, MAXLINE); //Lee la respuesta asumiendo que es el tamaño del archivo
				tam = atoi(buf);
				if(tam) {
					Rio_writen(clientfd, "OK\n", 3); //Envia OK al servidor para indicar que el cliente esta listo para recibir el archivo
					estado = RECIBIENDO_ARCHIVO; //Cambio de estado
				}else { //Si el tamaño es 0, se asume que el archivo no existe
					printf("Archivo no existe en el servidor\n");
					estado = EXIT;
				}
				break;
			case RECIBIENDO_ARCHIVO:
				printf("Recibiendo archivo de %d bytes\n",tam);
				filename[l] = '\0'; //Remover el \n antes de crear el archivo
				fp = Fopen(filename,"w"); //Crea el archvio con acceso de escritura
				filebuf = Malloc(tam);
				size_t n = Rio_readn(clientfd, filebuf, tam); //Lee el contenido del archivo enviado desde el servidor y lo guarda en un buffer
				if (n > 0){
					Fwrite(filebuf, 1, n, fp); //Escribe el archivo con el contenido del buffer
					printf("%lu bytes escritos en %s\n",n,filename);
				}else
					printf("Error en la transferencia de archivo");
			
				Free(filebuf);
				Fclose(fp);
				estado = EXIT;
				break;
			default:
				estado = EXIT;
		}
	}

	
	Free(filename);
	Close(clientfd);
	exit(0);
}
